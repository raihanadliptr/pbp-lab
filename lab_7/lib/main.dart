// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => Home();
}

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  String? getnama;
  String? getpekerjaan;
  String? getkonten;
  TextEditingController inputcontroller1 = new TextEditingController();
  TextEditingController inputcontroller2 = new TextEditingController();
  TextEditingController inputcontroller3 = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
      children: [
        Text('Nama'),
          TextFormField(
            controller: inputcontroller1,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
          ),
        Text('Pekerjaan'),
          TextFormField(
            controller: inputcontroller2,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
          ),
        Text('Konten'),
          TextFormField(
            controller: inputcontroller3,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Column (children: <Widget>[
            ElevatedButton(
              onPressed: () {
                setState(() {
                  getnama = inputcontroller1.text;
                  getpekerjaan = inputcontroller2.text;
                  getkonten = inputcontroller3.text;
                });
              },
              child: const Text('Submit'),
            ),
            Text(getnama != null? '$getnama':''),
            Text(getpekerjaan != null? '$getpekerjaan':''),
            Text(getkonten != null? '$getkonten':''),
            ],)
          ),
        ],
      ),
    );
  }
}

class Home extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(child: 
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
          ElevatedButton(
            // style: style,
            style: ElevatedButton.styleFrom(
                primary: Color(0XFF5F9EA0),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                textStyle: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold)),
            onPressed: () {
               Navigator.push(
                    context,
                    MaterialPageRoute<void>(
                      builder: (BuildContext context) {
                        return Scaffold(
                          appBar: AppBar(
                            title: const Text('Form Pandemi'),
                            backgroundColor: Color(0XFF5F9EA0),
                          ),
                          body: const MyCustomForm(),
                        );
                      },
                    ),
                  );
            },
            child: const Text('Add Info Pandemi'),
          ),
          Text(''),
          ElevatedButton(
            // style: style,
            style: ElevatedButton.styleFrom(
                primary: Color(0XFF5F9EA0),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                textStyle: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold)),
            onPressed: () {
               Navigator.push(
                    context,
                    MaterialPageRoute<void>(
                      builder: (BuildContext context) {
                        return Scaffold(
                          appBar: AppBar(
                            title: const Text('Form NIH BANG'),
                            backgroundColor: Color(0XFF5F9EA0),
                          ),
                          body: const MyCustomForm(),
                        );
                      },
                    ),
                  );
            },
            child: const Text('Add Tips Kesehatan'),
          ),
          Text(''),
          ElevatedButton(
            // style: style,
            style: ElevatedButton.styleFrom(
                primary: Color(0XFF5F9EA0),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                textStyle: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold)),
            onPressed: () {
               Navigator.push(
                    context,
                    MaterialPageRoute<void>(
                      builder: (BuildContext context) {
                        return Scaffold(
                          appBar: AppBar(
                            title: const Text('Form NIH BANG'),
                            backgroundColor: Color(0XFF5F9EA0),
                          ),
                          body: const MyCustomForm(),
                        );
                      },
                    ),
                  );         
            },
            child: const Text('Add Curahan Hati'),
          ),
          Text(''),
        ],)
      ,)
    );
  }
}
