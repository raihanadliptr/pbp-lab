from django.urls import path
from .views import friend_list
from .views import index

urlpatterns = [
    path('', index, name='index'),
    path('friend', friend_list, name='friend'),
    # TODO Add friends path using friend_list Views
]