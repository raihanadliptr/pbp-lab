from django.http import response
from django.shortcuts import render
from .models import Notes
from django.http.response import HttpResponse
from django.core import serializers 

# Create your views here.
def index(request):
    Note = Notes.objects.all().values()  # TODO Implement this
    response = {'Note': Note}
    return render(request, 'index_lab2.html', response)

def xml(self):
    data = serializers.serialize('xml', Notes.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(self):
    data = serializers.serialize('json', Notes.objects.all())
    return HttpResponse(data, content_type="application/json")
