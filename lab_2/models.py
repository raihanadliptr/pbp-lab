from django.db import models

# Create your models here.
class Notes(models.Model):
    fromWho = models.CharField(max_length=250)
    to = models.CharField(max_length=250)
    title = models.CharField(max_length=250)
    message = models.CharField(max_length=250)
