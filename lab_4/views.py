from django.shortcuts import render
from lab_2.models import Notes
from .forms import Notesform
from django.http import HttpResponseRedirect

def index(request):
    notes = Notes.objects.all().values()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab_4.html', response)

def add_notes(request):
    form = Notesform()
    context = {'form':form}
    if request.method == 'POST':
        form = Notesform(request.POST)
        if (form.is_valid()):
            form.save()
        return HttpResponseRedirect('/lab-4')
    return render(request, "lab4_form.html", context)

def notes_list(request):
    notes = Notes.objects.all().values()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)