from django.urls import path
from .views import index, add_notes, notes_list

urlpatterns = [
    path('', index, name='index'),
    path('add-notes', add_notes, name='addnotes'),
    path('notes-list', notes_list, name='notelist'),
    # TODO Add friends path using friend_list Views
]