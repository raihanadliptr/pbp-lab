1) Apa perbedaan JSON dan XML
--> Sebagai permulaan, JSON hanyalah format data sedangkan XML adalah bahasa markup. 
--> XML digunakan untuk menyimpan dan mengangkut data dari satu aplikasi ke aplikasi lain melalui Internet. JSON di sisi lain adalah format pertukaran data ringan yang jauh lebih     mudah bagi komputer untuk mengurai data yang sedang dikirim.
--> Format JSON secara komparatif jauh lebih mudah dibaca daripada dokumen berformat XML. 
--> Tetapi bagaimanapun, jika sebuah proyek memerlukan markup dokumen dan informasi metadata, lebih baik menggunakan XML.

    __________________________________________________________________________
   |__________________|___________XML___________|____________JSON_____________|
   |Bahasa            | Memiliki tag untuk      | Format yang ditulis         | 
   |                  | mendefinisikan elemen   | dalam JavaScript            |
   |Penyimpanan data  | Data XML disimpan       | Data disimpan seperti map   |
   |                  | sebagai tree structure  | dengan pasangan key value   |
   |Pengolahan        | Melakukan pemrosesan    | Tidak melakukan pemrosesan  |
   |                  | dan pemformatan         | atau perhitungan apa pun    |
   |                  | dokumen dan objek       |                             |
   |Kecepatan         | Besar dan lambat dalam  | Sangat cepat karena ukuran  |
   |                  | penguraian              | file sangat kecil           |
   |Keamanan          | Rentan terhadap         | Penguraian JSON aman hampir | 
   |                  | beberapa serangan       | sepanjang waktu kecuali     |
   |                  | karena perluasan        | jika JSONP digunakan, yang  |
   |                  | entitas eksternal dan   | dapat menyebabkan serangan  |
   |                  | validasi DTD diaktifkan | Cross-Site Request Forgery  |
   |                  | secara default          | (CSRF)                      |   
   |__________________|_________________________|_____________________________|
                           
2) Apa perbedaan HTML dan XML
--> XML == eXtensible Markup Language, HTML == Hypertext Markup Language.
--> XML berfokus pada transfer data, sedangkan HTML difokuskan pada penyajian data.
--> XML didorong konten, sedangkan HTML didorong oleh format.
--> XML == Case Sensitive, HTML == Case Insensitive
--> XML menyediakan dukungan namespaces, sementara HTML tidak menyediakan dukungan namespaces.
--> XML strict untuk tag penutup, sedangkan HTML tidak strict.
--> Tag XML dapat dikembangkan, sedangkan HTML memiliki tag terbatas.
--> Tag XML tidak ditentukan sebelumnya, sedangkan HTML memiliki tag yang telah ditentukan sebelumnya.